<CsoundSynthesizer>  

<CsOptions>

</CsOptions>

<CsInstruments>
sr              =               44100
ksmps           =               1
nchnls          =               1
0dbfs           =               1


;general values for fourier transform
gifftsiz  =         1024
gioverlap =         256
giwintyp  =         1 ;von hann window

                instr   1
	ibw = 4
	iscale = 2
ain	in
a60	areson	ain,	60,  ibw, iscale
a601	areson	a60,	60,  ibw, iscale
a120	areson	a601,	120, ibw, iscale
a1201	areson	a120,	120, ibw, iscale
a180	areson	a1201,	180, ibw, iscale
a1801	areson	a180,	180, ibw, iscale
a240	areson	a1801,	240, ibw, iscale
a300	areson	a240,	300, ibw, iscale
a360	areson	a300,	360, ibw, iscale
a420	areson	a360,	420, ibw, iscale
aout	balance a420, ain
	out aout
	endin

	instr 2
	ibw = 4
	iscale = 2
ain	in
a60	butterbr	ain,	60,  ibw
a60	butterbr	a60,	60,  ibw
a120	butterbr	a60,	120, ibw
a120	butterbr	a120,	120, ibw
a180	butterbr	a120,	180, ibw
a240	butterbr	a180,	240, ibw
a300	butterbr	a240,	300, ibw
a360	butterbr	a300,	360, ibw
a420	butterbr	a360,	420, ibw
a480	butterbr	a420,	480, ibw
a540	butterbr	a480,	540, ibw
	out 2*a540
	endin


instr 3
ain       inch      1 ;live input from channel 1
ahum      buzz      10,    60,       20,        1
fsig      pvsanal   ain,  gifftsiz, gioverlap, gifftsiz, giwintyp
fshum     pvsanal   ahum, gifftsiz, gioverlap, gifftsiz, giwintyp
kflag     pvsftw    fshum,4,        5
fsclean    pvstencil fsig, 0, 1, 4
aout pvsynth fsclean
              out aout
endin

instr 4
ain       inch      1 ;live input from channel 1
; ; ; ; ahum      buzz      10,    60,       20,        1
fsig      pvsanal   ain,  gifftsiz, gioverlap, gifftsiz, giwintyp
; ; ; ; fshum     pvsanal   ahum, gifftsiz, gioverlap, gifftsiz, giwintyp
; ; ; ; kflag     pvsftw    fshum,4,        5
; ; ; ; kflag     pvsftw    fshum,4,        5
fsclean    pvstencil fsig, 0, 1, 6
aout pvsynth fsclean
              out aout
endin


</CsInstruments>

<CsScore>
; Table #1, a sine wave.
f 1 0 16384 10 1
f 3 0 1024 10 1
f 4 0 1024 10 1 ; amp
f 5 0 1024 10 1 ; freq
f 6 0 1024 43 "noise.pvx" 1


i3 0 30 ; play an hour
</CsScore>
</CsoundSynthesizer>

